Tea flavors
======================

This document's purpose is to help the tea drinkers/lovers to discover the different flavors of tea.
It is a wheel of the different flavors you can encounter in a tea.
Flavor is the taste of something, especially the distinctive taste of something as it is experienced in the mouth. 


How to use it
-----
* Start from the center of the circle
* Recognize the first dominant note (can be different for the head, body or tail)
* From there locate what is the sub taste by going to the exterior of the circle


Language
-----

This document is in english, if you want to add another language, feel free to contribute !

![Alt text](/../master/Flavors_english_small.png?raw=true "Tea flavors in English")

Feedback
-----

I would be happy to discuss in order to have this wheel evolve and make it easier/more accurate. Feel free to write issues of contact me.


Files
-----
* Flavors_english : document, tea flavors wheel
* README.md : this file, contains the readme
* LICENSE : contains the licensing information


Public targeted
-----

This document was created as a personal help at first and meant to help the tea lovers to develop their senses and discover new sensations while appreciating a good beverage.


License
-----

This document is under the Creative Commons BY-NC-SA 4.0 license (see LICENSE file)

You are free to:

    Share — copy and redistribute the material in any medium or format
    Adapt — remix, transform, and build upon the material

    The licensor cannot revoke these freedoms as long as you follow the license terms.

Under the following terms:

    Attribution — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
    NonCommercial — You may not use the material for commercial purposes.
    ShareAlike — If you remix, transform, or build upon the material, you must distribute your contributions under the same license as the original.
    No additional restrictions — You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.


Inspirations and special thanks
-----

This flavors wheel is inspired by the wheel available in the wonderful book (I recommend having it):

    Camellia Sinensis THÉ: histoire, terroirs, saveurs
    Author : Maison de thé CAMELLIA SINENSIS
    Edition : Éditions de l'Homme (2009)
    ISBN : 9782761924726
    http://camellia-sinensis.com/fr/teaware/books/the-histoire-terroirs-saveurs

It evolved from personal experience and diverse sources on the web